import * as model from "../src/model.js";

var chai = require("chai");
var assert = chai.assert;

describe("model.js", function () {
    var time = 0;
    var id = 1;
    var x = 0;
    var y = 0;
    var angle = 45;
    var speed = 100;
    describe("MovingObject", function () {
        function checkState(o) {
            assert.equal(o.id, id);
            assert.equal(o.x, x);
            assert.equal(o.y, y);
            assert.equal(o.time, time);
            assert.equal(o.angle, angle);
            assert.equal(o.speed, speed);
        }
        it("should exists", function () {
            assert.isTrue(model.MovingObject != undefined)
        })
        it("new MovingObject(...) should instanciate an object", function () {
            var o = new model.MovingObject(id, x, y, time, angle, speed);
            checkState(o);
        });
        const xAt = [
            [0, 0],
            [10, 0.7071067811865476],
            [100, 7.0710678118654755],
            [1000, 70.71067811865476],
            [10000, 707.1067811865476],
            [100000, 671.067811865476],
        ];
        for (let i = 0; i < xAt.length; i++) {
            let xAtTime = xAt[i][0];
            let xAtValue = xAt[i][1];
            it(`xAt(${xAtTime}) should return ${xAtValue}`, function () {
                var o = new model.MovingObject(id, x, y, time, angle, speed);
                assert.approximately(o.xAt(xAtTime), xAtValue, 0.01);
                checkState(o);
            });
        }
        const yAt = [
            [0, 0],
            [10, 0.7071067811865476],
            [100, 7.0710678118654755],
            [1000, 70.71067811865476],
            [10000, 107.10678118654744],
            [100000, 471.06781186547505],
        ];
        for (let v = 0; v < yAt.length; v++) {
            let yAtTime = yAt[v][0];
            let yAtValue = yAt[v][1];
            it(`yAt(${yAtTime}) should return ${yAtValue}`, function () {
                var o = new model.MovingObject(id, x, y, time, angle, speed);
                assert.approximately(o.yAt(yAtTime), yAtValue, 0.01);
                checkState(o);
            });
        }
        it("sould raise an error when calling the draw abstract method", function () {
            var o = new model.MovingObject(id, x, y, time, angle, speed);
            assert.throws(() => o.draw({}, 0), Error, "Not implemented!");
            checkState(o);
        });
    });
    describe("Rocket", function () {
        const ctxAt = [
            [0, 0, 0, 2, 0, 0],
            [10, 0.7071067811865476, 0.7071067811865475],
            [100, 7.0710678118654755, 7.0710678118654755],
            [1000, 70.71067811865476, 70.71067811865476],
            [10000, 707.1067811865476, 107.10678118654744],
            [100000, 671.067811865476, 471.06781186547505],
        ];
        for (let i = 0; i < ctxAt.length; i++) {
            let ctxAtTime = ctxAt[i][0];
            let ctxAtX = ctxAt[i][1];
            let ctxAtY = ctxAt[i][2];
            it(`rocket.draw(ctx, ${ctxAtTime}) should call ctx.beginPath(), ctx.drawn(${ctxAtX}, ${ctxAtY}, ...) and ctx.stroke()`, function () {
                let ctx = {
                    beginPath: function () {
                        this.beginPathCall = true;
                    },
                    arc: function (x, y) {
                        this.x = x;
                        this.y = y;
                    },
                    stroke: function () {
                        this.strokeCall = true;
                    }
                }
                var o = new model.Rocket(id, x, y, time, angle, speed);
                o.draw(ctx, ctxAtTime)
                assert.isTrue(ctx.beginPathCall);
                assert.approximately(ctx.x, ctxAtX, 0.01);
                assert.approximately(ctx.y, ctxAtY, 0.01);
                assert.isTrue(ctx.strokeCall);
            });
        }
    });
});
