import * as util from "../src/util.js";

var chai = require("chai");
var assert = chai.assert;

describe("util.js", function () {
  describe("random(from, to)", function () {
    const from = -10;
    const to = 10;
    const values = 21;
    const executions = 1000;
    it("should return random integers within the specified range", function () {
      for (let e = 0; e < executions; e++) {
        let v = util.random(from, to);
        assert(v >= from);
        assert(v <= to);
      }
    });
    it("should generate all possible values given enough executions", function () {
      let set = new Set();
      for (let e = 0; e < executions; e++) {
        set.add(util.random(from, to));
      }
      assert.equal(set.size, values);
    });
    it("should fail with invalid arguments", function () {
      assert.throws(() => util.random(1.1, 2), Error, "Invalid arguments");
      assert.throws(() => util.random(1, 2.2), Error, "Invalid arguments");
      assert.throws(() => util.random("1", 2), Error, "Invalid arguments");
      assert.throws(() => util.random(1, "2"), Error, "Invalid arguments");
      assert.throws(
        () => util.random(undefined, 2),
        Error,
        "Invalid arguments"
      );
      assert.throws(
        () => util.random(1, undefined),
        Error,
        "Invalid arguments"
      );
    });
  });
  describe("randomColor()", function () {
    const executions = 1000;
    const hex = /#[0-9A-F]{3}/;
    it("should generate valid hex colors (/#[0-9A-F]{3}/)", function () {
      for (let e = 0; e < executions; e++) {
        assert(util.randomColor().match(hex));
      }
    });
    it("should generate different hex colors given a small number of executions", function () {
      let set = new Set();
      for (let e = 0; e < executions; e++) {
        set.add(util.randomColor().match(hex));
      }
      assert.equal(set.size, executions);
    });
  });
  const conversions = [
    [0, "0"],
    [22.5, "π / 8"],
    [45, "π / 4"],
    [90, "π / 2"],
    [180, "π"],
    [360, "π * 2"],
    [720, "π * 4"],
    [-180, "-π"]
  ];
  describe("radians(degrees)", function () {
    for (let v = 0; v < conversions.length; v++) {
      let degrees = conversions[v][0];
      let radians = conversions[v][1];
      it(`radians(${degrees}) should be equal to ${radians}`, function () {
        assert.equal(
          util.radians(degrees),
          eval(radians.replace("π", "Math.PI"))
        );
      });
    }
    it("should fail with invalid arguments", function () {
      assert.throws(() => util.radians("1"), Error, "Invalid arguments");
      assert.throws(() => util.radians(undefined), Error, "Invalid arguments");
    });
  });
  describe("degrees(radians)", function () {
    for (let v = 0; v < conversions.length; v++) {
      let degrees = conversions[v][0];
      let radians = conversions[v][1];
      it(`degrees(${radians}) should be equal to ${degrees}`, function () {
        assert.equal(
          util.degrees(eval(radians.replace("π", "Math.PI"))),
          degrees
        );
      });
    }
    it("should fail with invalid arguments", function () {
      assert.throws(() => util.degrees("1"), Error, "Invalid arguments");
      assert.throws(() => util.degrees(undefined), Error, "Invalid arguments");
    });
  });
  describe("adjacent(hypothenuse, degrees)", function () {
    const adjacents = [
      [5, 0, 5],
      [5, 22.5, 4.619],
      [5, 45, 3.535],
      [5, 90, 0],
      [5, 180, -5],
      [5, 360, 5],
      [5, -360, 5]
    ];
    for (let v = 0; v < adjacents.length; v++) {
      let hypothenuse = adjacents[v][0];
      let degrees = adjacents[v][1];
      let adjacent = adjacents[v][2];
      it(`adjacent(${hypothenuse}, ${degrees}) should be close to ${adjacent}`, function () {
        assert.closeTo(util.adjacent(hypothenuse, degrees), adjacent, 1e-3);
      });
    }
    it("should fail with invalid arguments", function () {
      assert.throws(() => util.adjacent(5, "0"), Error, "Invalid arguments");
      assert.throws(() => util.adjacent("5", 0), Error, "Invalid arguments");
      assert.throws(
        () => util.adjacent(5, undefined),
        Error,
        "Invalid arguments"
      );
      assert.throws(
        () => util.adjacent(undefined, 0),
        Error,
        "Invalid arguments"
      );
    });
  });
  const opposites = [
    [5, 0, 0],
    [5, 22.5, 1.913],
    [5, 45, 3.535],
    [5, 90, 5],
    [5, 180, 0],
    [5, 360, 0],
    [5, -360, 0]
  ];
  describe("opposite(hypothenuse, degrees)", function () {
    for (let v = 0; v < opposites.length; v++) {
      let hypothenuse = opposites[v][0];
      let degrees = opposites[v][1];
      let opposite = opposites[v][2];
      it(`opposite(${hypothenuse}, ${degrees}) should be close to ${opposite}`, function () {
        assert.closeTo(util.opposite(hypothenuse, degrees), opposite, 1e-3);
      });
    }
    it("should fail with invalid arguments", function () {
      assert.throws(() => util.opposite(5, "0"), Error, "Invalid arguments");
      assert.throws(() => util.opposite("5", 0), Error, "Invalid arguments");
      assert.throws(
        () => util.opposite(5, undefined),
        Error,
        "Invalid arguments"
      );
      assert.throws(
        () => util.opposite(undefined, 0),
        Error,
        "Invalid arguments"
      );
    });
  });
  const coordinates = [
    [0, 100, 0],
    [50, 100, 50],
    [100, 100, 0],
    [200, 100, 0],
    [300, 100, 0],
    [-50, 100, 50],
    [-100, 100, 0],
    [-200, 100, 0],
    [-300, 100, 0]
  ];
  describe("coord(coord, max)", function () {
    for (let v = 0; v < coordinates.length; v++) {
      let coord = coordinates[v][0];
      let max = coordinates[v][1];
      let value = coordinates[v][2];
      it(`coord(${coord}, ${max}) should be equal to ${value}`, function () {
        assert.equal(util.coord(coord, max), value);
      });
    }
  });
  const collisions = [
    [8, 8, 10, 10, 2, false],
    [9, 9, 10, 10, 2, true],
    [10, 10, 10, 10, 2, true],
    [11, 11, 10, 10, 2, true],
    [12, 12, 10, 10, 2, false]
  ];
  describe("collision(pX, pY, cX, cY, cR)", function () {
    for (let v = 0; v < collisions.length; v++) {
      let pX = collisions[v][0];
      let pY = collisions[v][1];
      let cX = collisions[v][2];
      let cY = collisions[v][3];
      let cR = collisions[v][4];
      let value = collisions[v][5];
      it(`collision(${pX}, ${pY}, ${cX}, ${cY}, ${cR}) should be equal to ${value}`, function () {
        assert.equal(util.collision(pX, pY, cX, cY, cR), value);
      });
    }
  });
});
