import { width, height } from "./constants.js";
import { adjacent, opposite, coord, radians } from "./util.js";


class MovingObject {

  constructor(id, x, y, time, angle, speed) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.time = time;
    this.angle = angle;
    this.speed = speed;
  }

  xAt(time) {
    let deltaTime = (time - this.time) / 1000
    let distance = deltaTime*this.speed;
    let adj = adjacent(distance,this.angle) + this.x
    return coord(adj,width);
  }

  yAt(time) {
    let deltaTime = (time - this.time) / 1000;
    let distance = deltaTime*this.speed;
    let opp = opposite(distance,this.angle) + this.y
    return coord(opp,height);
  }

  draw(ctx, time) {
      throw new Error('Not implemented!');
  }
}

class Asteroid extends MovingObject {

  constructor(id, x, y, time, angle, speed, radius) {
    super(id, x, y, time, angle, speed)
    this.radius = radius;
  }

  draw(ctx, time) {
    ctx.strokeStyle = "#FFFFFF";
    ctx.beginPath();
    ctx.arc(this.xAt(time), this.yAt(time), this.radius, 0, 2 * Math.PI);
    ctx.stroke();
  }

}

class Spaceship extends MovingObject {

  constructor(id, x, y, time, angle, speed, color) {
    super(id, x, y, time, angle, speed)
    this.color = color;
  }

  draw(ctx, time) {
    ctx.save();
    ctx.strokeStyle = "#FFFFFF";
    ctx.translate(this.xAt(time), this.yAt(time));
    // rotating around the center of the spaceship
    ctx.rotate(radians(this.angle));
    ctx.beginPath();
    ctx.moveTo(15, 0);
    ctx.lineTo(-15, 10);
    ctx.lineTo(-10, 0);
    ctx.lineTo(-15, -10);
    ctx.closePath();
    ctx.stroke();
    ctx.restore();
  }

}

class Rocket extends MovingObject {

  constructor(id, x, y, time, angle, speed, spaceshipID) {
    super(id, x, y, time, angle, speed)
    this.spaceshipID = spaceshipID;
  }

  draw(ctx, time) {
    ctx.strokeStyle  = '#FFFFFF'
    ctx.beginPath();
    ctx.arc(this.xAt(time), this.yAt(time), 2, 0, 2 * Math.PI);
    ctx.stroke()
  }
}
export { MovingObject, Rocket, Asteroid, Spaceship };