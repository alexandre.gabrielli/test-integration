/**
 * Return true if the input is a Number.
 * @param {*} input
 */
function isNum(input) {
  return typeof input === "number";
}

/**
 * Return true if the input is an Integer.
 * @param {*} input
 */
function isInt(input) {
  return Number.isInteger(input);
}

/**
 * Generate random number between f and t.
 *
 * @param {Number} from
 * @param {Number} to
 */
function random(from, to) {
  if (!isInt(from) || !isInt(to)) {
    throw new Error("Invalid arguments");
  }
  return Math.floor(Math.random() * (to - from + 1) + from);
}

/**
 * Generate random colors.
 */
function randomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 3; i++) {
    color += letters[random(0, 15)];
  }
  return color;
}

/**
 * Converts degrees to radians.
 *
 * @param {Number} degrees
 */
function radians(degrees) {
  if (!isNum(degrees)) {
    throw new Error("Invalid arguments");
  }
  return degrees * (Math.PI / 180);
}

/**
 * Convert radians to degrees.
 * @param {Number} radians
 */
function degrees(radians) {
  if (!isNum(radians)) {
    throw new Error("Invalid arguments");
  }
  return radians * (180 / Math.PI);
}

/**
 * Computes the adjacent  of a triangle from an hypotenuse and an angle.
 *
 * @param {Number} hypotenuse
 * @param {Number} degrees
 */
function adjacent(hypotenuse, degrees) {
  if (!isNum(hypotenuse) || !isNum(degrees)) {
    throw new Error("Invalid arguments");
  }
  return Math.cos(radians(degrees)) * hypotenuse;
}

/**
 * Computes the opposite of a triangle from an hypotenuse and an angle.
 *
 * @param {Number} hypotenuse
 * @param {Number} angle
 */
function opposite(hypotenuse, degrees) {
  if (!isNum(hypotenuse) || !isNum(degrees)) {
    throw new Error("Invalid arguments");
  }
  return Math.sin(radians(degrees)) * hypotenuse;
}

/**
 * Computes the coordinates on a plan.
 *
 * @param {Number} coord
 * @param {Number} max
 */
function coord(coord, max) {
  if (!isNum(coord) || !isNum(max)) {
    throw new Error("Invalid arguments");
  }
  if (coord >= 0) {
    return coord % max;
  } else {
    return ((coord % max) + max) % max;
  }
}

/**
 * Check for collisions between a point an a circle.
 *
 * @param {Number} pX
 * @param {Number} pY
 * @param {Number} cX
 * @param {Number} cY
 * @param {Number} cR
 */
function collision(pX, pY, cX, cY, cR) {
  if (!isNum(pX) || !isNum(pY) || !isNum(cX) || !isNum(cY) || !isNum(cR)) {
    throw new Error("Invalid arguments");
  }
  return Math.pow(pX - cX, 2) + Math.pow(pY - cY, 2) < Math.pow(cR, 2);
}

export {
  adjacent,
  collision,
  coord,
  degrees,
  opposite,
  radians,
  random,
  randomColor
};
