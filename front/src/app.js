
import { Asteroid, Rocket, Spaceship } from "./model.js";
import {width, height} from "./constants.js";

// current time t
let time = new Date().getTime();

let rocket = new Rocket(
  1, //id
  200, // x
  200, //y
  time, // time
  45, // angle
  50, // speed
  2 // spaceship id
);

let spaceship = new Spaceship(
  2, // id
  100, // x
  100, // y
  time, // time
  45, // angle
  50, // speed
  '#FFF' // color
);

let asteroid = new Asteroid(
  3, // id
  300, // x
  300, // y
  time, // time
  45, // angle
  50, // speed
  50 // radius
);

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

function gameLoop() {
  time = new Date().getTime();
  // clearing drawing context
  ctx.clearRect(0, 0, width, height);
  // drawing the objects
  asteroid.draw(ctx, time);
  spaceship.draw(ctx, time);
  rocket.draw(ctx, time);

  window.requestAnimationFrame(gameLoop);
}

// start gaming loop
window.requestAnimationFrame(gameLoop);

