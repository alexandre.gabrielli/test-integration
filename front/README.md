# Exercise 3

You are asked to implement the domain model of the video game in javascript using classes.

![Domain Model](domain_model.png)

The Mocha test cases are mainly provided as implementation guidelines and cover the constructor, the `xAt(time)`, `yAt(time)` and `draw(ctx, time)` methods of the MovingObject class, and the `draw(ctx, time)` method of the Rocket class. 
The `draw` methods of the Spaceship and Asteroid classes are not covered, but their effect will be checked during the correction.
[Transformations](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Transformations) can be used to draw the spaceship.

Notice that the design of this model is constrained by the end result that includes multiplayer and networking features. 
For instance, the properties of the objects represent their state at a given time, not their current position in the canvas.
The rendering loop and the draw methods, use this state to draw the objects at a later time with the `xAt(time)` and `yAt(time)` methods.
This kind of time decoupling is necessary to synchronize the state of the game on the network in a non-blocking fashion. 

When implementing the `xAt` and `yAt` methods, use the functions implemented in `util.js` and recall that the distance is the speed (expressed in pixels per seconds) multiplied by the duration. 
In addition, use the `width` and `height` constants in the `constant.js` file that correspond to the size of the canvas (800x600px).

The exercise can more easily be achieved by doing the tasks in the following order.
- Create the MovingObject class and check wether it passes the unit tests.
- Create the Rocket class and check wether it passes the unit tests.
- Retrieve the canvas object from the DOM and get its drawing context.
- Implement the rendering loop and visually verify that the rocket moves in the browser.
- Replicate the process to implement the `draw` methods of the Asteroid and Spaceship classes.

All your modifications should take place in the files `src/app.js` and `src/model.js`.
For the corrections, we will check that you project passes the tests and visually verify in the browser that the three objects are displayed and moving according to the following gif.

![Result](result.gif)
